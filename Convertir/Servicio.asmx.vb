﻿Imports System.ComponentModel
Imports System.Web.Services
Imports System.Math
Imports System.Web.Services.Protocols

    ' Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
    ' <System.Web.Script.Services.ScriptService()> _
    <System.Web.Services.WebService(Namespace:="http://Convertir1.somee.com")>
    <System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class Servicio
    Inherits System.Web.Services.WebService

    <WebMethod()>
    Public Function CeFa(dato1 As Double) As Double
        Dim fa = ((dato1) * (1.8)) + 32
        fa = Round(fa, 2)
        Return fa
    End Function

    <WebMethod()>
    Public Function CeKel(dato1 As Double) As Double
        Dim kel = dato1 + 273.15
        kel = Round(kel, 2)
        Return kel
    End Function

    <WebMethod()>
    Public Function CeRa(dato1 As Double) As Double
        Dim ra = (dato1 * 1.8) + 491.67
        ra = Round(ra, 2)
        Return ra
    End Function

    <WebMethod()>
    Public Function CeRe(dato1 As Double) As Double
        Dim re = dato1 * 0.8
        re = Round(re, 2)
        Return re
    End Function

    <WebMethod()>
    Public Function FaCe(dato1 As Double) As Double
        Dim Ce = (dato1 - 32) / 1.8
        Ce = Round(Ce, 2)
        Return Ce
    End Function

    <WebMethod()>
    Public Function FaKel(dato1 As Double) As Double
        Dim kel = ((5 / 9) * (dato1 - 32)) + 273.15
        kel = Round(kel, 2)
        Return kel
    End Function
    <WebMethod()>
    Public Function FaRa(dato1 As Double) As Double
        Dim ra = dato1 + 459.67
        ra = Round(ra, 2)
        Return ra
    End Function
    <WebMethod()>
    Public Function FaRe(dato1 As Double) As Double
        Dim re = (4 / 9) * (dato1 - 32)
        re = Round(re, 2)
        Return re
    End Function

    <WebMethod()>
    Public Function KelCel(dato1 As Double) As Double
        Dim cel = (dato1 - 273.15)
        cel = Round(cel, 2)
        Return cel
    End Function

    <WebMethod()>
    Public Function KelFa(dato1 As Double) As Double
        Dim fa = ((1.8) * (dato1 - 273.15)) + 32
        fa = Round(fa, 2)
        Return fa
    End Function

    <WebMethod()>
    Public Function KelRa(dato1 As Double) As Double
        Dim ra = dato1 * 1.8
        ra = Round(ra, 2)
        Return ra
    End Function
    <WebMethod()>
    Public Function KelRe(dato1 As Double) As Double
        Dim re = (0.8) * (dato1 - 273.15)
        re = Round(re, 2)
        Return re
    End Function

    <WebMethod()>
    Public Function RaCe(dato1 As Double) As Double
        Dim ce = (dato1 - 491.67) / 1.8
        ce = Round(ce, 2)
        Return ce
    End Function
    <WebMethod()>
    Public Function RaFa(dato1 As Double) As Double
        Dim ce = (dato1 - 459.67)
        ce = Round(ce, 2)
        Return ce
    End Function
    <WebMethod()>
    Public Function RaKel(dato1 As Double) As Double
        Dim ce = dato1 / 1.8
        ce = Round(ce, 2)
        Return ce
    End Function

    <WebMethod()>
    Public Function RaRe(dato1 As Double) As Double
        Dim ce = (dato1 - 491.67) * (4 / 9)
        ce = Round(ce, 2)
        Return ce
    End Function

    <WebMethod()>
    Public Function ReCe(dato1 As Double) As Double
        Dim ce = dato1 * 1.25
        ce = Round(ce, 2)
        Return ce
    End Function
    <WebMethod()>
    Public Function ReFa(dato1 As Double) As Double
        Dim ce = (dato1 * (9 / 4)) + 32
        ce = Round(ce, 2)
        Return ce
    End Function
    <WebMethod()>
    Public Function ReKel(dato1 As Double) As Double
        Dim ce = (dato1 * 1.25) + 273.15
        ce = Round(ce, 2)
        Return ce
    End Function

    <WebMethod()>
    Public Function ReRa(dato1 As Double) As Double
        Dim ce = (dato1 * (9 / 4)) + 491.67
        ce = Round(ce, 2)
        Return ce
    End Function


End Class